import image from '../assets/BG-1.jpeg';
import { SiCakephp } from 'react-icons/si'

export default function Home(){
    return(
        <div style=
        {{ backgroundImage: `url(${image})`, backgroundRepeat:'no-repeat', backgroundPosition: 'center', backgroundSize: 'cover', height: '90vh', width: '90vw'}}>
        <h1 style={{textAlign: 'center', position: 'absolute', top:'50%', left:'50%', transform: 'translate(-50%, -50%)'}}><SiCakephp />Welcome to JK's CAKES & BAKES</h1>
        
        </div>

    )
}