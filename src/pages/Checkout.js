import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Checkout(){
	const {user} = useContext(UserContext);
	const { productId } = useParams();
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price , setPrice] = useState(0);

	useEffect(() => {
		console.log(productId);

		fetch(`http://localhost:3000/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])

	const order = (productId, quantity, totalAmount) => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				totalAmount: totalAmount,
                productId: productId,
                quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				Swal.fire({
					position: 'top-end',
					title: "Successfully ordered",
					icon: "success",
					showConfirmButton: false,
					timer: 1500
				});
				<Navigate to='/menu'/>
			}else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	
		
	}

	return(
		<>
		<h1>Checkout</h1>
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card>
						<Card.Body>
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>
					        
					        {user.id !== null
					        	?
					        	<Button variant="primary" onClick={() => order(productId)}>Order</Button>
					        	:
					        	<Button as={Link} to="/login" variant="danger">Log in to Order</Button>
					    	}

					    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		</>
	)	
}
